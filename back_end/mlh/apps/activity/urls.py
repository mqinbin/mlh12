from django.conf.urls import url
from django.contrib import admin
from . import views
urlpatterns = [
   url('^activitys/$' , views.ActivityViewSet.as_view({"get":"list"})),
   url('^activitys/(?P<pk>\d+)$' , views.ActivityViewSet.as_view({"get":"retrieve"})),
]
